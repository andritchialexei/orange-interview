This is an android project that represents a practice task for the interview for "Orange" company.

It's an app that displays lists of movies and their details using TheMovieDB open API: "https://developers.themoviedb.org/3"

Language: Java

Architecture: Model-View-Presenter

Third-party libraries: Retrofit, Picasso, ButterKnife