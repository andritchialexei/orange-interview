package md.orange.orangeinterview;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class OrangeApplication extends Application {
}
