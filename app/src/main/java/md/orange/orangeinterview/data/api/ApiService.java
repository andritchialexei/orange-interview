package md.orange.orangeinterview.data.api;

import io.reactivex.Observable;
import md.orange.orangeinterview.data.models.MovieDetailsResponse;
import md.orange.orangeinterview.data.models.MovieListResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET("3/movie/popular")
    Observable<MovieListResponse> getPopularMovies(@Query("page") int page);

    @GET("/3/movie/top_rated")
    Observable<MovieListResponse> getTopRatedMovies(@Query("page") int page);

    @GET("3/movie/{movie_id}")
    Observable<MovieDetailsResponse> getMovieDetails(@Path("movie_id") int movieId);
}
