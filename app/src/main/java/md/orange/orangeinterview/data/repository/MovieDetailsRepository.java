package md.orange.orangeinterview.data.repository;

import io.reactivex.Observable;
import io.reactivex.Observer;
import md.orange.orangeinterview.data.models.MovieDetailsResponse;
import retrofit2.Callback;

public interface MovieDetailsRepository {
    Observable<MovieDetailsResponse> getMovieDetails(int movieId);
}
