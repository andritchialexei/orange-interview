package md.orange.orangeinterview.data.repository;

import io.reactivex.Observable;
import md.orange.orangeinterview.data.models.MovieListResponse;

public interface PopularMoviesRepository {
    Observable<MovieListResponse> getPopularMovies(int page);
}
