package md.orange.orangeinterview.data.repository;

import io.reactivex.Observable;
import io.reactivex.Observer;
import md.orange.orangeinterview.data.models.MovieListResponse;

public interface TopRatedMoviesRepository {
    Observable<MovieListResponse> getTopRatedMovies(int page);
}
