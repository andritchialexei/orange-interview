package md.orange.orangeinterview.data.repository.impl;

import javax.inject.Inject;

import io.reactivex.Observable;
import md.orange.orangeinterview.data.api.ApiService;
import md.orange.orangeinterview.data.models.MovieDetailsResponse;
import md.orange.orangeinterview.data.repository.MovieDetailsRepository;

public class MovieDetailsRepositoryImpl implements MovieDetailsRepository {

    private final ApiService service;

    @Inject
    public MovieDetailsRepositoryImpl(ApiService service) {
        this.service = service;
    }

    @Override
    public Observable<MovieDetailsResponse> getMovieDetails(int movieId) {
        return service.getMovieDetails(movieId);
    }
}
