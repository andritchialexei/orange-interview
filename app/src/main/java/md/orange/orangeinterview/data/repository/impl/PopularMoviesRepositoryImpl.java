package md.orange.orangeinterview.data.repository.impl;

import javax.inject.Inject;

import io.reactivex.Observable;
import md.orange.orangeinterview.data.api.ApiService;
import md.orange.orangeinterview.data.models.MovieListResponse;
import md.orange.orangeinterview.data.repository.PopularMoviesRepository;

public class PopularMoviesRepositoryImpl implements PopularMoviesRepository {

    private final ApiService service;

    @Inject
    public PopularMoviesRepositoryImpl(ApiService service) {
        this.service = service;
    }

    @Override
    public Observable<MovieListResponse> getPopularMovies(int page) {
        return service.getPopularMovies(page);
    }
}
