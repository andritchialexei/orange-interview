package md.orange.orangeinterview.data.repository.impl;

import javax.inject.Inject;

import io.reactivex.Observable;
import md.orange.orangeinterview.data.api.ApiService;
import md.orange.orangeinterview.data.models.MovieListResponse;
import md.orange.orangeinterview.data.repository.TopRatedMoviesRepository;

public class TopRatedMoviesRepositoryImpl implements TopRatedMoviesRepository {

    private final ApiService service;

    @Inject
    public TopRatedMoviesRepositoryImpl(ApiService service) {
        this.service = service;
    }


    @Override
    public Observable<MovieListResponse> getTopRatedMovies(int page) {
        return service.getTopRatedMovies(page);
    }
}
