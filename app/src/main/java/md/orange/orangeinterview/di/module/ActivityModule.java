package md.orange.orangeinterview.di.module;

import androidx.fragment.app.Fragment;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.android.scopes.FragmentScoped;
import md.orange.orangeinterview.ui.movie_details.MovieDetailsFragment;
import md.orange.orangeinterview.ui.popular_movies.PopularMoviesFragment;
import md.orange.orangeinterview.ui.top_rated_movies.TopRatedMoviesFragment;

@Module
@InstallIn(FragmentComponent.class)
public class ActivityModule {
    @Provides
    @FragmentScoped
    PopularMoviesFragment providesPopularMoviesFragment(Fragment fragment) {
        return (PopularMoviesFragment) fragment;
    }

    @Provides
    @FragmentScoped
    TopRatedMoviesFragment providesTopRatedMoviesFragment(Fragment fragment) {
        return (TopRatedMoviesFragment) fragment;
    }

    @Provides
    @FragmentScoped
    MovieDetailsFragment providesMovieDetailsFragment(Fragment fragment) {
        return (MovieDetailsFragment) fragment;
    }
}
