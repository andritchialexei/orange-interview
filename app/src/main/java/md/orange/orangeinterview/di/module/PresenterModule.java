package md.orange.orangeinterview.di.module;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.android.scopes.FragmentScoped;
import md.orange.orangeinterview.data.repository.MovieDetailsRepository;
import md.orange.orangeinterview.data.repository.PopularMoviesRepository;
import md.orange.orangeinterview.data.repository.TopRatedMoviesRepository;
import md.orange.orangeinterview.ui.movie_details.MovieDetailsContract;
import md.orange.orangeinterview.ui.movie_details.MovieDetailsPresenterImpl;
import md.orange.orangeinterview.ui.popular_movies.PopularMoviesContract;
import md.orange.orangeinterview.ui.popular_movies.PopularMoviesPresenterImpl;
import md.orange.orangeinterview.ui.top_rated_movies.TopRatedMoviesContract;
import md.orange.orangeinterview.ui.top_rated_movies.TopRatedMoviesPresenterImpl;

@Module
@InstallIn(FragmentComponent.class)
public class PresenterModule {

    @Provides
    @FragmentScoped
    PopularMoviesContract.Presenter providesPopularMoviesPresenter(PopularMoviesContract.View view, PopularMoviesRepository repository) {
        return new PopularMoviesPresenterImpl(view, repository);
    }

    @Provides
    @FragmentScoped
    TopRatedMoviesContract.Presenter providesTopRatedMoviesPresenter(TopRatedMoviesContract.View view, TopRatedMoviesRepository repository) {
        return new TopRatedMoviesPresenterImpl(view, repository);
    }

    @Provides
    @FragmentScoped
    MovieDetailsContract.Presenter providesMovieDetailsPresenter(MovieDetailsContract.View view, MovieDetailsRepository repository) {
        return new MovieDetailsPresenterImpl(view, repository);
    }
}


