package md.orange.orangeinterview.di.module;


import dagger.Binds;
import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import md.orange.orangeinterview.data.repository.MovieDetailsRepository;
import md.orange.orangeinterview.data.repository.PopularMoviesRepository;
import md.orange.orangeinterview.data.repository.TopRatedMoviesRepository;
import md.orange.orangeinterview.data.repository.impl.MovieDetailsRepositoryImpl;
import md.orange.orangeinterview.data.repository.impl.PopularMoviesRepositoryImpl;
import md.orange.orangeinterview.data.repository.impl.TopRatedMoviesRepositoryImpl;

@Module
@InstallIn(ApplicationComponent.class)
public abstract class RepositoryModule {
    @Binds
    abstract PopularMoviesRepository bindPopularMoviesRepository(PopularMoviesRepositoryImpl repository);

    @Binds
    abstract TopRatedMoviesRepository bindTopRatedMoviesRepository(TopRatedMoviesRepositoryImpl repository);

    @Binds
    abstract MovieDetailsRepository bindMovieDetailsRepository(MovieDetailsRepositoryImpl repository);
}
