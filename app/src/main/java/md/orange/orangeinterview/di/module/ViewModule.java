package md.orange.orangeinterview.di.module;


import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.android.scopes.FragmentScoped;
import md.orange.orangeinterview.ui.movie_details.MovieDetailsContract;
import md.orange.orangeinterview.ui.movie_details.MovieDetailsFragment;
import md.orange.orangeinterview.ui.popular_movies.PopularMoviesContract;
import md.orange.orangeinterview.ui.popular_movies.PopularMoviesFragment;
import md.orange.orangeinterview.ui.top_rated_movies.TopRatedMoviesContract;
import md.orange.orangeinterview.ui.top_rated_movies.TopRatedMoviesFragment;

@Module
@InstallIn(FragmentComponent.class)
public class ViewModule {
    @Provides
    @FragmentScoped
    PopularMoviesContract.View providesPopularMoviesView(PopularMoviesFragment fragment) {
        return fragment;
    }

    @Provides
    @FragmentScoped
    TopRatedMoviesContract.View providesTopRatedMoviesView(TopRatedMoviesFragment fragment) {
        return fragment;
    }

    @Provides
    @FragmentScoped
    MovieDetailsContract.View providesMovieDetailsView(MovieDetailsFragment fragment) {
        return fragment;
    }
}
