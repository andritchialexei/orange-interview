package md.orange.orangeinterview.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.hilt.android.AndroidEntryPoint;
import md.orange.orangeinterview.R;
import md.orange.orangeinterview.utils.ViewPagerNoSwipe;
import md.orange.orangeinterview.ui.popular_movies.PopularMoviesFragment;
import md.orange.orangeinterview.ui.top_rated_movies.TopRatedMoviesFragment;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.view_pager)
    ViewPagerNoSwipe viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        MainPagerAdapter adapter = new MainPagerAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager, true);

        tabLayout.getTabAt(0).setText(getResources().getString(R.string.popular));
        tabLayout.getTabAt(1).setText(getResources().getString(R.string.top_rated));


    }

    private static class MainPagerAdapter extends FragmentStatePagerAdapter {

        final PopularMoviesFragment popularMoviesFragment;
        final TopRatedMoviesFragment topRatedMoviesFragment;

        public MainPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);

            popularMoviesFragment = new PopularMoviesFragment();
            topRatedMoviesFragment = new TopRatedMoviesFragment();
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 1) {
                return topRatedMoviesFragment;
            }
            return popularMoviesFragment;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}