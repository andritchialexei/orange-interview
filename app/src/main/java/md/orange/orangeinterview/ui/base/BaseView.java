package md.orange.orangeinterview.ui.base;

public interface BaseView {
    void showLoading();

    void hideLoading();

    void showError();
}
