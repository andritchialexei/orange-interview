package md.orange.orangeinterview.ui.movie_details;

import md.orange.orangeinterview.data.models.MovieDetailsResponse;
import md.orange.orangeinterview.ui.base.BaseView;

public interface MovieDetailsContract {

    interface View extends BaseView {
        void showMovieDetails(MovieDetailsResponse movieDetails);
    }

    interface Presenter {
        void getMovieDetails(int movieId);
    }
}
