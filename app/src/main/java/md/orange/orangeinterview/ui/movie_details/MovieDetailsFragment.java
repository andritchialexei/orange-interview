package md.orange.orangeinterview.ui.movie_details;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.hilt.android.AndroidEntryPoint;
import md.orange.orangeinterview.R;
import md.orange.orangeinterview.utils.Utils;
import md.orange.orangeinterview.data.models.MovieDetailsResponse;

@AndroidEntryPoint
public class MovieDetailsFragment extends Fragment implements MovieDetailsContract.View {

    public static final String TAG = MovieDetailsFragment.class.getSimpleName();

    @BindView(R.id.tvMovieTitle)
    TextView tvMovieTitle;
    @BindView(R.id.tvMovieOverview)
    TextView tvMovieOverview;
    @BindView(R.id.progress_bar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.ivPoster)
    ImageView ivPoster;
    @BindView(R.id.tvError)
    TextView tvError;

    @Inject
    MovieDetailsContract.Presenter presenter;

    private int movieId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull android.view.View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null
                && getActivity().getIntent() != null
                && getActivity().getIntent().getExtras() != null) {
            movieId = getActivity().getIntent().getExtras().getInt("movieId");
        } else {
            Log.e(TAG, "movieId is not correct");
        }

        progressBarLayout.setVisibility(android.view.View.VISIBLE);
        presenter.getMovieDetails(movieId);

    }

    @Override
    public void showMovieDetails(MovieDetailsResponse movieDetails) {
        tvError.setVisibility(View.GONE);
        tvMovieTitle.setVisibility(View.VISIBLE);
        tvMovieOverview.setVisibility(View.VISIBLE);
        ivPoster.setVisibility(View.VISIBLE);

        tvMovieTitle.setText(movieDetails.getTitle());
        tvMovieOverview.setText(movieDetails.getOverview());
        Picasso.get().load(Utils.POSTER_BASE_PATH_ORIGINAL + movieDetails.getPosterPath())
                .placeholder(R.drawable.ic_movie_placeholder)
                .fit()
                .into(ivPoster);
    }

    @Override
    public void showLoading() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        tvError.setVisibility(View.VISIBLE);
        tvMovieTitle.setVisibility(View.GONE);
        tvMovieOverview.setVisibility(View.GONE);
        ivPoster.setVisibility(View.GONE);
    }
}