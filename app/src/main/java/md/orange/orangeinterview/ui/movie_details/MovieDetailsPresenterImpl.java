package md.orange.orangeinterview.ui.movie_details;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import md.orange.orangeinterview.data.models.MovieDetailsResponse;
import md.orange.orangeinterview.data.repository.MovieDetailsRepository;

public class MovieDetailsPresenterImpl implements MovieDetailsContract.Presenter {

    private final MovieDetailsContract.View view;
    private final MovieDetailsRepository repository;

    @Inject
    public MovieDetailsPresenterImpl(MovieDetailsContract.View view, MovieDetailsRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void getMovieDetails(int movieId) {
        repository.getMovieDetails(movieId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MovieDetailsResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        view.showLoading();
                    }

                    @Override
                    public void onNext(@NonNull MovieDetailsResponse movieDetailsResponse) {
                        view.showMovieDetails(movieDetailsResponse);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.hideLoading();
                        view.showError();
                    }

                    @Override
                    public void onComplete() {
                        view.hideLoading();
                    }
                });
    }
}
