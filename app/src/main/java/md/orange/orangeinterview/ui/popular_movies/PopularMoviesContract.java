package md.orange.orangeinterview.ui.popular_movies;

import java.util.List;

import md.orange.orangeinterview.data.models.Movie;
import md.orange.orangeinterview.ui.base.BaseView;

public interface PopularMoviesContract {

    interface View extends BaseView {
        void showPopularMovies(List<Movie> movieList);

        void onMovieClick(int movieId);
    }

    interface Presenter {
        void getPopularMovies();

        void onScrollDownLoadMoreMovies();
    }
}
