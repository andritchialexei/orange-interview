package md.orange.orangeinterview.ui.popular_movies;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.hilt.android.AndroidEntryPoint;
import md.orange.orangeinterview.R;
import md.orange.orangeinterview.data.models.Movie;
import md.orange.orangeinterview.ui.movie_details.MovieDetailsActivity;
import md.orange.orangeinterview.utils.EndlessScrollListener;

@AndroidEntryPoint
public class PopularMoviesFragment extends Fragment implements PopularMoviesContract.View,
        EndlessScrollListener.ScrollToBottomListener {

    @BindView(R.id.rvPopularMovies)
    RecyclerView rvPopularMovies;
    @BindView(R.id.progress_bar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.tvError)
    TextView tvError;

    @Inject
    PopularMoviesContract.Presenter presenter;

    private PopularMoviesAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_popular_movies, container, false);
    }

    @Override
    public void onViewCreated(@NonNull android.view.View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        rvPopularMovies.setLayoutManager(linearLayoutManager);
        adapter = new PopularMoviesAdapter(this);
        rvPopularMovies.setAdapter(adapter);

        EndlessScrollListener endlessScrollListener = new EndlessScrollListener(linearLayoutManager, this);
        rvPopularMovies.addOnScrollListener(endlessScrollListener);
    }

    @Override
    public void onResume() {
        super.onResume();

        presenter.getPopularMovies();
    }

    @Override
    public void showPopularMovies(List<Movie> movieList) {
        rvPopularMovies.setVisibility(View.VISIBLE);
        tvError.setVisibility(View.GONE);

        if (adapter == null) {
            adapter = new PopularMoviesAdapter(this);
            adapter.setList(movieList);
            rvPopularMovies.setAdapter(adapter);

        } else {
            adapter.addAll(movieList);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onMovieClick(int movieId) {
        Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
        intent.putExtra("movieId", movieId);
        startActivity(intent);
    }

    @Override
    public void onScrollToBottom() {
        presenter.onScrollDownLoadMoreMovies();
    }

    @Override
    public void showError() {
        tvError.setVisibility(View.VISIBLE);
        rvPopularMovies.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBarLayout.setVisibility(View.GONE);
    }
}