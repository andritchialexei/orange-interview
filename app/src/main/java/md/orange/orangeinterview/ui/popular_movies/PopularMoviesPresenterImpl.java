package md.orange.orangeinterview.ui.popular_movies;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import md.orange.orangeinterview.data.models.MovieListResponse;
import md.orange.orangeinterview.data.repository.PopularMoviesRepository;

public class PopularMoviesPresenterImpl implements PopularMoviesContract.Presenter {

    private final PopularMoviesContract.View view;
    private final PopularMoviesRepository repository;

    private int page = 1;

    @Inject
    public PopularMoviesPresenterImpl(PopularMoviesContract.View view, PopularMoviesRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void getPopularMovies() {
        repository.getPopularMovies(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MovieListResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        view.showLoading();
                    }

                    @Override
                    public void onNext(@NonNull MovieListResponse movieListResponse) {
                        view.showPopularMovies(movieListResponse.getMovieList());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.hideLoading();
                        view.showError();
                    }

                    @Override
                    public void onComplete() {
                        view.hideLoading();
                    }
                });
    }

    @Override
    public void onScrollDownLoadMoreMovies() {
        view.showLoading();
        page++;
        getPopularMovies();
    }
}
