package md.orange.orangeinterview.ui.top_rated_movies;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import md.orange.orangeinterview.R;
import md.orange.orangeinterview.utils.Utils;
import md.orange.orangeinterview.data.models.Movie;


public class TopRatedMoviesAdapter extends RecyclerView.Adapter<TopRatedMoviesAdapter.ViewHolder> {

    private final TopRatedMoviesContract.View fragment;
    private List<Movie> movieList;

    public TopRatedMoviesAdapter(TopRatedMoviesContract.View fragment) {
        this.fragment = fragment;
        movieList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Movie movie = movieList.get(position);

        Picasso.get().load(Utils.POSTER_BASE_PATH_ORIGINAL + movie.getPosterPath())
                .placeholder(R.drawable.ic_movie_placeholder)
                .fit()
                .into(holder.ivPoster);

        holder.movie = movie;
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public void setList(List<Movie> movieList) {
        this.movieList = movieList;
        notifyDataSetChanged();
    }

    public void addAll(List<Movie> productList) {
        this.movieList.addAll(productList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements android.view.View.OnClickListener {

        @BindView(R.id.ivPoster)
        ImageView ivPoster;

        private Movie movie;

        public ViewHolder(@NonNull android.view.View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(android.view.View view) {
            fragment.onMovieClick(movie.getId());
        }
    }
}
