package md.orange.orangeinterview.ui.top_rated_movies;

import java.util.List;

import md.orange.orangeinterview.data.models.Movie;
import md.orange.orangeinterview.ui.base.BaseView;

public interface TopRatedMoviesContract {

    interface View extends BaseView {
        void showTopRatedMovies(List<Movie> movieList);

        void onMovieClick(int movieId);
    }

    interface Presenter {
        void getTopRatedMovies();

        void onScrollDownLoadMoreMovies();
    }
}
