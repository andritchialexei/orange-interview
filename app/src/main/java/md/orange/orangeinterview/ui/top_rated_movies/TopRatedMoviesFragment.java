package md.orange.orangeinterview.ui.top_rated_movies;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.hilt.android.AndroidEntryPoint;
import md.orange.orangeinterview.R;
import md.orange.orangeinterview.data.models.Movie;
import md.orange.orangeinterview.ui.movie_details.MovieDetailsActivity;
import md.orange.orangeinterview.utils.EndlessScrollListener;

@AndroidEntryPoint
public class TopRatedMoviesFragment extends Fragment implements TopRatedMoviesContract.View,
        EndlessScrollListener.ScrollToBottomListener {

    @BindView(R.id.rvTopRatedMovies)
    RecyclerView rvTopRatedMovies;
    @BindView(R.id.progress_bar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.tvError)
    TextView tvError;

    @Inject
    TopRatedMoviesContract.Presenter presenter;

    private TopRatedMoviesAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_top_rated_movies, container, false);
    }

    @Override
    public void onViewCreated(@NonNull android.view.View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        rvTopRatedMovies.setLayoutManager(linearLayoutManager);
        adapter = new TopRatedMoviesAdapter(this);
        rvTopRatedMovies.setAdapter(adapter);

        EndlessScrollListener endlessScrollListener = new EndlessScrollListener(linearLayoutManager, this);
        rvTopRatedMovies.addOnScrollListener(endlessScrollListener);
    }

    @Override
    public void onResume() {
        super.onResume();

        presenter.getTopRatedMovies();
    }

    @Override
    public void showTopRatedMovies(List<Movie> movieList) {
        rvTopRatedMovies.setVisibility(View.VISIBLE);
        tvError.setVisibility(View.GONE);

        if (adapter == null) {
            adapter = new TopRatedMoviesAdapter(this);
            adapter.setList(movieList);
            rvTopRatedMovies.setAdapter(adapter);

        } else {
            adapter.addAll(movieList);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onMovieClick(int movieId) {
        Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
        intent.putExtra("movieId", movieId);
        startActivity(intent);
    }

    @Override
    public void onScrollToBottom() {
        presenter.onScrollDownLoadMoreMovies();
    }

    @Override
    public void showLoading() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        tvError.setVisibility(View.VISIBLE);
        rvTopRatedMovies.setVisibility(View.GONE);
    }
}