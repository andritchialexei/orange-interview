package md.orange.orangeinterview.ui.top_rated_movies;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import md.orange.orangeinterview.data.models.MovieListResponse;
import md.orange.orangeinterview.data.repository.TopRatedMoviesRepository;

public class TopRatedMoviesPresenterImpl implements TopRatedMoviesContract.Presenter {

    private final TopRatedMoviesContract.View view;
    private final TopRatedMoviesRepository repository;

    private int page = 1;

    @Inject
    public TopRatedMoviesPresenterImpl(TopRatedMoviesContract.View view, TopRatedMoviesRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void getTopRatedMovies() {
        repository.getTopRatedMovies(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MovieListResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        view.showLoading();
                    }

                    @Override
                    public void onNext(@NonNull MovieListResponse movieListResponse) {
                        view.showTopRatedMovies(movieListResponse.getMovieList());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.hideLoading();
                        view.showError();
                    }

                    @Override
                    public void onComplete() {
                        view.hideLoading();
                    }
                });
    }

    @Override
    public void onScrollDownLoadMoreMovies() {
        page++;
        getTopRatedMovies();
    }
}
