package md.orange.orangeinterview.ui.movie_details;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;
import md.orange.orangeinterview.data.models.MovieDetailsResponse;
import md.orange.orangeinterview.data.repository.MovieDetailsRepository;
import test.RxSchedulersOverrideRule;

import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieDetailsPresenterImplTest {

    @Rule
    public RxSchedulersOverrideRule schedulersOverrideRule = new RxSchedulersOverrideRule();
    @Mock
    private MovieDetailsRepository repository;
    @Mock
    private MovieDetailsContract.View view;
    @Mock
    private MovieDetailsResponse movieDetailsResponse;

    private MovieDetailsContract.Presenter presenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        presenter = new MovieDetailsPresenterImpl(view, repository);
    }

    @Test
    public void getMovieDetails() {
        // Given
        when(repository.getMovieDetails(anyInt())).thenReturn(Observable.just(movieDetailsResponse));

        // When
        presenter.getMovieDetails(anyInt());

        // Then
        verify(view).hideLoading();
        verify(view).showMovieDetails(movieDetailsResponse);
    }

    @Test
    public void fail_getTopRatedMovies() {
        // Given
        when(repository.getMovieDetails(anyInt())).thenReturn(Observable.error(UnknownError::new));

        // When
        presenter.getMovieDetails(anyInt());

        // Then
        verify(view).hideLoading();
        verify(view).showError();
    }
}