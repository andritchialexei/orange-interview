package md.orange.orangeinterview.ui.popular_movies;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;
import md.orange.orangeinterview.data.models.MovieListResponse;
import md.orange.orangeinterview.data.repository.PopularMoviesRepository;
import test.RxSchedulersOverrideRule;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PopularMoviesPresenterImplTest {

    @Rule
    public RxSchedulersOverrideRule schedulersOverrideRule = new RxSchedulersOverrideRule();
    @Mock
    private PopularMoviesContract.View view;
    @Mock
    private PopularMoviesRepository repository;
    @Mock
    private MovieListResponse movieListResponse;

    private PopularMoviesContract.Presenter presenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        presenter = new PopularMoviesPresenterImpl(view, repository);
    }

    @Test
    public void getPopularMovies() {
        // Given
        when(repository.getPopularMovies(anyInt())).thenReturn(Observable.just(movieListResponse));

        // When
        presenter.getPopularMovies();

        // Then
        verify(view).hideLoading();
        verify(view).showPopularMovies(movieListResponse.getMovieList());
    }

    @Test
    public void fail_getPopularMovies() {
        // Given
        when(repository.getPopularMovies(anyInt())).thenReturn(Observable.error(UnknownError::new));

        // When
        presenter.getPopularMovies();

        // Then
        verify(view).hideLoading();
        verify(view).showError();
    }
}