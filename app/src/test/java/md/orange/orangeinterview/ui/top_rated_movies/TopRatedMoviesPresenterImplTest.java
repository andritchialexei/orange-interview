package md.orange.orangeinterview.ui.top_rated_movies;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;
import md.orange.orangeinterview.data.models.MovieListResponse;
import md.orange.orangeinterview.data.repository.TopRatedMoviesRepository;
import test.RxSchedulersOverrideRule;

import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TopRatedMoviesPresenterImplTest {

    @Rule
    public RxSchedulersOverrideRule schedulersOverrideRule = new RxSchedulersOverrideRule();
    @Mock
    private TopRatedMoviesRepository repository;
    @Mock
    private TopRatedMoviesContract.View view;

    @Mock
    private MovieListResponse movieListResponse;

    private TopRatedMoviesContract.Presenter presenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        presenter = new TopRatedMoviesPresenterImpl(view, repository);
    }

    @Test
    public void getTopRatedMovies() {
        // Given
        when(repository.getTopRatedMovies(anyInt())).thenReturn(Observable.just(movieListResponse));

        // When
        presenter.getTopRatedMovies();

        // Then
        verify(view).hideLoading();
        verify(view).showTopRatedMovies(movieListResponse.getMovieList());
    }

    @Test
    public void fail_getTopRatedMovies() {
        // Given
        when(repository.getTopRatedMovies(anyInt())).thenReturn(Observable.error(UnknownError::new));

        // When
        presenter.getTopRatedMovies();

        // Then
        verify(view).hideLoading();
        verify(view).showError();
    }
}